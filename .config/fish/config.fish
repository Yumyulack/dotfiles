pfetch
set TERM "xterm-256color"                         # Sets the terminal type
### AUTOCOMPLETE AND HIGHLIGHT COLORS ###
set fish_color_normal brcyan
set fish_color_autosuggestion '#7d7d7d'
set fish_color_command brcyan
set fish_color_error '#ff6c6b'
set fish_color_param brcya

#Rust support for fish
set -l rustup_path $HOME/.cargo/bin

set -q CARGO_HOME
and set -l rustup_path $CARGO_HOME/bin

test -d $rustup_path
and contains -- $rustup_path $fish_user_paths
or set fish_user_paths $fish_user_paths $rustup_path

function _halostatue_fish_rust_uninstall -e halostatue_fish_rust_uninstall
    set -l rustup_path $HOME/.cargo/bin
    set -q CARGO_HOME
    and set -l rustup_path $CARGO_HOME/bin

    set -l i (contains -i -- $rustup_path $fish_user_paths)
    and set -e fish_user_paths[$i]

    functions -e (status function)
end

# Changing "ls" to "exa"
alias ls='exa -al --color=always --group-directories-first' # my preferred listing
alias la='exa -a --color=always --group-directories-first'  # all files and dirs
alias ll='exa -l --color=always --group-directories-first'  # long format
alias lt='exa -aT --color=always --group-directories-first' # tree listing
alias l.='exa -a | egrep "^\."'
# Adding bare git for dotfiles gitlab
alias config='/usr/bin/git --git-dir=$HOME/dotfiles/ --work-tree=$HOME'
#Startship
starship init fish | source
