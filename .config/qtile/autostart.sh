#!/usr/bin/env bash
#
# Using bash in the shebang rather than /bin/sh, which should
# be avoided as non-POSIX shell users (fish) may experience errors.

#lxsession &
xfsettingsd &
picom --experimental-backends &
nitrogen --restore &
urxvtd -q -o -f &
steam &
flameshot &
#volumeicon &
#nm-applet &
